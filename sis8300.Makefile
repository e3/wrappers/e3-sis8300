#
#  Copyright (c) 2018 - 2021  European Spallation Source ERIC
#
#  The program is free software: you can redistribute
#  it and/or modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation, either version 2 of the
#  License, or any newer version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
#
#
# Author  : joaopaulomartins
#           Jeong Han Lee
#           Gabriel Fedel
# email   : joaopaulomartins@esss.se
#           jeonghan.lee@gmail.com
#           gabrielfedel@ess.eu
#


where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile


EXCLUDE_ARCHS += linux-ppc64e6500

# print cc1plus: warning: unrecognized command line option ‘-Wno-format-truncation’ with lower gcc 7
USR_CXXFLAGS += -Wno-format-truncation -std=c++11


APP:=sis8300App
APPDB:=$(APP)/Db
APPSRC:=$(APP)/src


HEADERS += $(wildcard $(APPSRC)/*.h)

SOURCES += $(APPSRC)/sis8300Device.cpp
SOURCES += $(APPSRC)/sis8300RegisterChannelGroup.cpp
SOURCES += $(APPSRC)/sis8300RegisterChannel.cpp
SOURCES += $(APPSRC)/sis8300AIChannelGroup.cpp
SOURCES += $(APPSRC)/sis8300AIChannel.cpp
SOURCES += $(APPSRC)/sis8300AOChannelGroup.cpp
SOURCES += $(APPSRC)/sis8300AOChannel.cpp


ifeq (linux-ppc64e6500, $(findstring linux-ppc64e6500,$(T_A)))
USR_INCLUDES += -I$(SDKTARGETSYSROOT)/usr/include/libxml2
else ifeq (linux-corei7-poky, $(findstring linux-corei7-poky,$(T_A)))
USR_INCLUDES += -I$(SDKTARGETSYSROOT)/usr/include/libxml2
else
USR_INCLUDES += -I/usr/include/libxml2
endif

USR_LIBS += xml2



TEMPLATES += $(wildcard $(APPDB)/*.template)
TEMPLATES += $(APPDB)/sis8300.db
TEMPLATES += $(APPDB)/sis8300Register.db
TEMPLATES += $(APPDB)/sis8300noAO.db
TEMPLATES += $(APPDB)/sis8300RTMnoAO.db



USR_DBFLAGS += -I . -I ..
USR_DBFLAGS += -I $(EPICS_BASE)/db
USR_DBFLAGS += -I $(APPDB)



SUBS=$(wildcard $(APPDB)/*.substitutions)
TMPS=$(wildcard $(APPDB)/*.template)



#
.PHONY: vlibs
vlibs:
#
